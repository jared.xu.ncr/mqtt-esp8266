# main.py

def sub_cb(topic, msg):
  print((topic, msg))
  if topic == b'build/gcp/mon':
     if msg == b'ON':
        print('Turn on the Klaxon and Red Alert!')
        sound_klaxon()
     if msg == b'OFF':
        print('Turn off the Klaxon and Red Alert!')
        flash_alert()

def connect_and_subscribe():
  global client_id, mqtt_server, topic_sub
  client = MQTTClient(client_id, mqtt_server)
  client.set_callback(sub_cb)
  client.connect()
  client.subscribe(topic_sub)
  print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_sub))
  return client

def restart_and_reconnect():
  print('Failed to connect to MQTT broker. Reconnecting...')
  time.sleep(10)
  machine.reset()

def sound_klaxon():
  pin = machine.Pin(0, machine.Pin.OUT)
  times = 10
  start = 1
  while start < times:
      pin.value(1)
      time.sleep(2)
      start += 1
  pin.value(0)


def flash_alert():
  pin = machine.Pin(1, machine.Pin.OUT)
  times = 20
  start = 1
  while start < times:
      pin.value(1)
      time.sleep(1)
      start += 1
  pin.value(0)


try:
  client = connect_and_subscribe()
except OSError as e:
  restart_and_reconnect()

while True:
  try:
    client.check_msg()
  except OSError as e:
    restart_and_reconnect()
