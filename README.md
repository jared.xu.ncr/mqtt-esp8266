# mqtt-esp8266

the mqtt subscriber in esp8266,

Install the micropython runtime first following [here](http://docs.micropython.org/en/v1.9.3/esp8266/esp8266/tutorial/intro.html#getting-the-firmware).

Install ampy following [here](https://github.com/pycampers/ampy).

Using ampy to upload your files from `device` folder to your chip using ```ampy --port /dev/ttyUSB0 --baud 115200 put your_file.py```
 
